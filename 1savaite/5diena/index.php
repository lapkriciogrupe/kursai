<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Spalvyno karpymas</title>
    <link href="https://fonts.googleapis.com/css?family=Alegreya+SC:700|Trocchi" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <section class="pirma">
        <header>
            <nav>
                <img src="img/spalvynas-logo.png" alt="Spalvynas logo">
                <ul>
                    <li><a href="#">Apie mus</a></li>
                    <li><a href="#">Paslaugos</a></li>
                    <li><a href="#">Kodėl mes?</a></li>
                    <li><a href="#">Kaip mes dirbame?</a></li>
                    <li><a href="#">Kontaktai</a></li>
                </ul>
            </nav>
        </header>
        <ul>
            <li><a href="tel:+370 555 55555"><img src="img/ragelis.png" alt="Spalvynas telefono numeris">+370 555 55555</a></li>
        </ul>
        <h1>Dažų gamyba ir prekyba</h1>
        <h2>Automobiliams, laivams, metalinems konstrukcijoms ir t.t.</h2>
        <img src="img/arrow.png" alt="Rodyklė">
    </section>
    <section class="antra">
        <h1><img src="img/spalvynas-logo-tamsus.png" alt="">tai - </h1>
        <div class="container">
            <div class="row">
                <div class="col-md-4 text-center">
                    <h2>patirtis</h2>
                    <p>Dirbame daugiau kaip 22 metus</p>
                </div>
                <div class="col-md-4 text-center">
                    <h2>tęstinumas</h2>
                    <p>Esame padarę dažus perdažimui daugiau nei 20000 daužtų mašinų</p>
                </div>
                <div class="col-md-4 text-center">
                    <h2>patikimumas</h2>
                    <p>50 pastovių klientų su kuriais dirbame daugiau kaip 20 metų</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 text-center">
                    <h2>darbštumas</h2>
                    <p>Vien per pastaruosius metus turėjome 500 klientų</p>
                </div>
                <div class="col-md-4 text-center">
                    <h2>efektyvumas</h2>
                    <p>Greito užsakymo galimybė. Įprastų užsakymų vykdymas iki 1 dienos</p>
                </div>
                <div class="col-md-4 text-center">
                    <h2>įvairovė</h2>
                    <p>Sunaudojame 100kg dažų naujų spalvų gamybai</p>
                </div>
            </div>
        </div>
    </section>
</body>
</html>