<?php
/**
 * Created by PhpStorm.
 * User: Osvaldas
 * Date: 11/12/17
 * Time: 15:57
 */
//$x = $_GET['X'];
//$y = $_GET['Y'];
//$rezultatas = $x + $y;
//
//echo "Atsiųstų skaičių $x ir $y suma lygi $rezultatas";

$svoris = $_GET['svoris'];
$ugis = $_GET['ugis'];

$kmi = round($svoris/pow(($ugis/100),2),2);
$sql_uzklausa = "create database test";

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Php uzduotys be db</title>
</head>
<body>
    <h1>KMI:<?php echo $kmi; ?></h1>
    <?php
    switch($kmi){
        case ($kmi<18.5):
            echo "Per mažas svoris/mitybos nepakankamumas";
            break;
        case (24.9>$kmi && $kmi>18.5):
            echo "Normalus svoris, normali kūno masė";
            break;
        case (29.9>$kmi && $kmi>25):
            echo "Antsvoris";
            break;
        case (34.9>$kmi && $kmi>30):
            echo "I laipsnio nutukimas";
            break;
        case (39.9>$kmi && $kmi>35):
            echo "II laipsnio nutukimas";
            break;
        case (40>$kmi):
            echo "III laipsnio nutukimas";
            break;
    }

    ?>

</body>
</html>
